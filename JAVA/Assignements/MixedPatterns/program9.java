import java.io.*;

class pattern{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader( new InputStreamReader ( System.in));
                System.out.print("Enter number of rows: ");
                int rows = Integer.parseInt(br.readLine());


                for(int i = rows ; i >= 1 ; i--){
			
			
                	int num = 1;
			char ch = (char)(64+i);

                        for(int j = i ; j >= 1 ; j--){
				
				if(rows % 2 == 0){
					if(i % 2 == 0)
                                		System.out.print(num++ + " ");
					else
						System.out.print(ch--+" ");
				}else{
				
					if(i % 2 == 1)
                                		System.out.print(num++ + " ");
					else
						System.out.print(ch--+" ");
				}

                        }


                        System.out.println();
                }
        }
}
