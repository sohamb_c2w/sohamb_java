import java.io.*;

class pattern{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader( new InputStreamReader ( System.in));
                System.out.print("Enter number of rows: ");
                int rows = Integer.parseInt(br.readLine());

                int num = (rows*(rows+1))/2;

                for(int i = 1 ; i <= rows ; i++){

                        for(int j = i ; j <= rows ; j++){

                                System.out.print((char)(64+num--) + " ");

                        }

                        System.out.println();
                }
        }
}
