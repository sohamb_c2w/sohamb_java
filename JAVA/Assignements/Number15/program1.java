import java.io.*;

class Fact{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter a number: ");
		int num = Integer.parseInt(br.readLine());

		int temp = 1;
		while(num>=temp){
			
			if(num%temp == 0)
				System.out.print(temp + " ");
			
			temp++;
		}
	}
}
