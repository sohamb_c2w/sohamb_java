import java.io.*;

class Factorial{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter a number: ");
                int num = Integer.parseInt(br.readLine());

                int temp = 1;
		int fact = 1;
                while(num >= temp){

                        fact *= temp;

                        temp++;
                }

                System.out.println("factorial of " + num + " is " + fact);
        }
}
