import java.io.*;

class RevNum{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter a number: ");
                int num = Integer.parseInt(br.readLine());

                int rem = 0;
                int ReNum = 0;
		while(num > 1){

                       rem = num % 10 ;
		       ReNum = rem + ReNum*10 ;
		       num = num/10;
                }

                System.out.println("Reverse of "+ num + " is " + ReNum);
        }
}
