import java.io.*;

class Palindrome{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter a number: ");
                int num = Integer.parseInt(br.readLine());

                int revNum = 0;
		int rem = 0;
		int Newnum = num;
                while(Newnum >= 1){
			
			rem = Newnum % 10;
			revNum = revNum*10 + rem;
			Newnum = Newnum/10;
                }


                if(num == revNum)
                        System.out.println( num + " is a palindrome Number");
                else
                        System.out.println( num + " is not a palindrome Number");
        }
}
