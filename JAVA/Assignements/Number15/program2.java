import java.io.*;

class Prime{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter a number: ");
                int num = Integer.parseInt(br.readLine());

                int temp = 1;
		int count = 0;

                while(num/2 >= temp){

                        if(num%temp == 0)
                                count++;

                        temp++;
                }

		if(count == 1)
			System.out.println(num + " is a prime Number");
		else
			System.out.println(num + " is not a prime Number");
        }
}
