class ODdEven{
        public static void main(String[] args){

                int num = 2569185;
                int rem = 0;
		int OddSum = 0;
		int EvenProduct = 1;
                while(num > 0){

                        rem = num % 10 ;
                        num = num / 10;

                        if(rem % 2 == 0){
                                EvenProduct *= rem;
                        }else{
				OddSum += rem;
			}
                }
	

		System.out.println("Sum of odd digits: "+OddSum);
        
		System.out.println("Product of even digits: "+EvenProduct);
	}
}
