class OddProduct{
        public static void main(String[] args){

                int num = 256985;
                int rem = 0;
		int product = 1;
                while(num > 0){

                        rem = num % 10 ;
                        num = num / 10;

                        if(rem % 2 != 0){
                        	
				product *= rem;
			}
                }

                System.out.println(product);
        }
}
