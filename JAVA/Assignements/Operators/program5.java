class operator{
        public static void main(String[] args){
		
		int num1 = 19;		
		// 0000 0000 0000 0000 0000 0000 0001 0011	
		int num2 = 41;		
		// 0000 0000 0000 0000 0000 0000 0010 1001
                 System.out.println(num1 & num2); 	// 0000 0001 (1)
                  System.out.println(num1 | num2);	// 0011 1011 (59)
		  System.out.println(num1 ^ num2);      // 0011 1010 (58)
		  System.out.println(num1 << 3); 
		 // 0000 0000 0000 0000 0000 0000 1001 1000  (152)
		  System.out.println(num2 >> 4);
		 // 0000 0000 0000 0000 0000 0000 0000 0010  (2)
		  System.out.println(num2 >>> 4);
		 // (2)
        }
}
