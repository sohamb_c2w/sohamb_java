class operator{
        public static void main(String[] args){
	
		int num1 = 29;  // 0001 1101
		int num2 = 21;  // 0001 0101

		// Arithmetic Operators
		//
                 System.out.println(num1 + num2);	//50
                 System.out.println(num1 - num2);	//8
        	 System.out.println(num1 * num2);	//609
		  System.out.println(num1 / num2);	//1
		   System.out.println(num1 % num2); 	//8

		//unary
		 System.out.println(num1++);	//29
		  System.out.println(num2--);	//21
		   System.out.println(++num2);	//31
		    System.out.println(--num1);	//19
		
		//Relational
		int stud1_marks = 98;
		int stud2_marks = 67;

		 System.out.println(stud1_marks == stud2_marks);	// false
		System.out.println(stud1_marks != stud2_marks);		//true
		System.out.println(stud1_marks > stud2_marks);		//true
		System.out.println(stud1_marks < stud2_marks);		//false
		System.out.println(stud1_marks >= stud2_marks);		//true
		System.out.println(stud1_marks <= stud2_marks);		//false

		//Assignment 
		int burger = 23;
		System.out.println(burger++);	//23
		System.out.println(burger--);	//23
		System.out.println(++burger);	//24
		System.out.println(--burger);	//23
		
		//Logical
		System.out.println((burger>60) && (stud1_marks >=90));	//false
		System.out.println((burger>60) || (stud1_marks >=90));     //true
		 System.out.println(!(burger>60));     //true
	
		//bitwise
		int no1 = 21;			//0001 0101
		int no2 = 13;			//0000 1101
		 System.out.println(no1 & no2);	//0000 0101
		 System.out.println(no1 | no2);	//0001 1101
		 System.out.println(no1 ^ no2);	//0001 1000
		 System.out.println(no1>>2);	//0000 0101
		 System.out.println(no2<<2);	//0011 0100
		 System.out.println(no1>>>2);	//0000 0001	
	}	

}	
