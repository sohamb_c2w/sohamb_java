class operator{
	public static void main(String[] args){
		int x = 30;
		int y = 25;

		System.out.println(++x + y++);	//56
		// 31+25 x=31 y=26
		System.out.println(x++ + ++y + ++x + ++x );	//125
		//31+27+33+34 x=34 y=27
		System.out.println(y++ + ++x + ++x);	//98
		//27+35+36 x=36 y=28

	}
}
