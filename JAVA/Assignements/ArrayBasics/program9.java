import java.io.*;
class ArrayBasics{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter array size: ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element at arr["+i+"]: ");
                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println();

                System.out.println("Array Elements are: ");

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(" | " + arr[i]);
                }

                System.out.println();

                System.out.println("Odd indexed Array elements is: ");

                int sum = 0 ;

                for(int i = 0 ; i < arr.length ; i++){
			
			if(i % 2 == 1)
                       		System.out.println(arr[i] + " is and odd indexed element placed at index " + i );
                }

               
        }
}
