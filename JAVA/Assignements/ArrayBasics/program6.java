import java.io.*;

class ArrayBasics{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter array size: ");
                int size = Integer.parseInt(br.readLine());

                char arr[] = new char[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element at arr["+i+"]: ");
                        arr[i] = (char)br.read();
			br.skip(1);

                }

                System.out.println();

                System.out.println("Array Elements are: ");

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(" | " + arr[i]);

                }

                System.out.println();


        }
}
