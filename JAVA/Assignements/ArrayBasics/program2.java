

class ArrayBasics{

        public static void main(String[] args) {


                int arr[] = new int[]{10,20,30,40,50,60,70,80,90,100};


                System.out.println();

                System.out.println("Array Elements are: ");

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(" | " + arr[i]);
                }

                System.out.println();
        }
}
