import java.io.*;

class ArrayBasics{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter Number of overs: ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter runs Scored in over["+(i+1)+"]: ");
                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println();

                System.out.println("Runs Scored in each overs are: ");

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.println("Runs scored in over" + (i+1) + " is " + arr[i]);
                }

                System.out.println();

 	}
}
