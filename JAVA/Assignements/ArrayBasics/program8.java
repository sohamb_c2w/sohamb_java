import java.io.*;

class ArrayBasics{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter Count of employee's: ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter age of emplyoee "+ (i+1) +": ");
                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println();

                System.out.println("Employee's Age is: ");

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(" | " + arr[i]);
                }

                System.out.println();

        }
}
