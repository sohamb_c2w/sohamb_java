import java.io.*;
class ArrayBasics{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter array size: ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element at arr["+i+"]: ");
                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println();

                System.out.println("Array Elements are: ");

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(" | " + arr[i]);
                }

                System.out.println();

                System.out.println("Array elements less than 10 are: ");


                for(int i = 0 ; i < arr.length ; i++){

                       if(arr[i] < 10 )
			       System.out.println(arr[i] + " is less than 10");
                }

                System.out.println();
        }
}
