import java.io.*;

class pattern{

        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter number of rows: ");
                int rows = Integer.parseInt(br.readLine());

		

		int start = rows;
		
		for(int i =  1 ; i<=rows ; i++){
			
			int x = start;

			for(int j = 1 ; j <= rows-i+1; j++){
				
				if(j % 2 == 1)
					System.out.print(x-- + " ");
				else
					System.out.print((char)(96 + x--) + " ");
			}

			start--;
			System.out.println();
		}
		
					

	}
}
