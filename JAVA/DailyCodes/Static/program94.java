class Demo{

        int x = 10;
        static int y = 20;

	void fun(){
		System.out.println("in fun method");
	}

	static void run(){
                System.out.println("in run method");
        }

        public static void main(String[] args){

                Demo obj =  new Demo();
		obj.fun();
		obj.run();
                System.out.println(obj.x);
                System.out.println(obj.y);
        }

}
