class SwitchDemo{
        public static void main(String[] args){

                boolean x = true;
                System.out.println("BEFORE SWITCH");

                switch(x){

			case true:
				System.out.println("True");
                                break;

                        case false:
                                System.out.println("False");
                                break;
			
			default:
                                System.out.println("in default state");


                }

                System.out.println("END SWITCH");

        }
}
