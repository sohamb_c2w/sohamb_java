class SwitchDemo{
        public static void main(String[] args){

                String friends = "Kanha";
                System.out.println("BEFORE SWITCH");

                switch(friends){

                        case "Ashish":
                                System.out.println("TCS");
                                break;

                        case "Kanha":
                                System.out.println("BMC");
                                break;

                        case "Rahul":
                                System.out.println("PTC");
                                break;

                        default:
                                System.out.println("in default state");


                }

                System.out.println("END SWITCH");

        }
}
