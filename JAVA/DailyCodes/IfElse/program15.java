class SwitchDemo{
        public static void main(String[] args){

                float num = 1.5f;
                System.out.println("BEFORE SWITCH");

                switch(num){

                        case 1.5:
                                System.out.println("1.5");


                        case 2.5:
                                System.out.println("2.5");


                        case 3.5:
                                System.out.println("3.5");


                        default:
                                System.out.println("in default state");


                }

                System.out.println("END SWITCH");

        }
}

