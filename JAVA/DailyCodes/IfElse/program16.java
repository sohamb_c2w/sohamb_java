class SwitchDemo{
        public static void main(String[] args){

                char ch = 'B';
                System.out.println("BEFORE SWITCH");

                switch(ch){

                        case 'A':
                                System.out.println("A");
				break;
				
			case 'B':
                                System.out.println("B");
				break;

                        case 'C':
                                System.out.println("C");
				break;

                        default:
                                System.out.println("in default state");


                }

                System.out.println("END SWITCH");

        }
}
