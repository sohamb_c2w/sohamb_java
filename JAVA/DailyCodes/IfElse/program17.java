class SwitchDemo{
        public static void main(String[] args){

                char ch = 'B';
                System.out.println("BEFORE SWITCH");

                switch(ch){

                        case 'A':
                                System.out.println("A");
                                break;

                        case 65:
                                System.out.println("65");
                                break;

                        case 'C':
                                System.out.println("C");
                                break;
			
			
			case 66:
				System.out.println("66");
				break;

                        default:
                                System.out.println("in default state");


                }

                System.out.println("END SWITCH");

        }
}
