class operator{
	public static void main(String[] args){
		
		int x=10;
		int y=23;

		x+=y;		// x=33 y=23
		y-=x;		// x=33 y=-10
		x*=y;		// x=-330 y=-10
		y+=y;		// x=-330 y=-20
		 System.out.println(x);		// -330
		  System.out.println(y);	// -20
	}

}

