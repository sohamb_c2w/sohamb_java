class operator{
	public static void main(String[] args){
		
		int x=12;
		int y=11;
		System.out.println((--x >= y++) && (--x < y++));	//true
		System.out.println("x = "+x);					//10
		System.out.println("y = "+y);					//13

		int a=1;
		int b=-1;
		System.out.println((--a < b++) && (a-- >= --b));   	//false
		System.out.println("a = "+a);                      	//0
		System.out.println("b = "+b);				//0
									

		System.out.println((++a >= b--) && (a++ != ++b));	// true
		System.out.println("a = "+a);				//2
		System.out.println("b = "+b);				//0
									
		System.out.println(((x+=4) >= y++) && ((x-=2) <= --y));    	// true
		System.out.println("x = "+x);				//12
		System.out.println("y = "+y);				//13
	}

}
