import java.util.*;

class Arraydemo{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter array size: ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Size of an array is : " + arr.length);

		int sum = 0 ;

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter array element: ");
                        arr[i]=sc.nextInt();
			sum += arr[i];
                }

                System.out.println("Array elements are: ");

                for(int i = 0 ; i< arr.length ; i++){

                        System.out.println(arr[i]);
		}

                System.out.println("sum of Array elements is: "+sum);
        }
}
