import java.util.*;

class Arrays2D {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter number of rows: ");
		int rows = sc.nextInt();

		System.out.print("Enter number of columns: ");
		int cols = sc.nextInt();

		int arr[][] = new int[rows][cols];	

		System.out.println();
		System.out.println("Enter array elements: ");
		System.out.println();

		for(int i = 0 ; i < arr.length ; i++) {
			
			for(int j = 0 ; j < arr[i].length ; j++) {
				
				System.out.print("Enter element ["+i+"]["+j+"]:");
				arr[i][j] = sc.nextInt();
			}
		}

		System.out.println("Array elements are: ");
		System.out.println();

		for(int i = 0 ; i < arr.length ; i++) {
			
			for(int j = 0 ; j < arr[i].length ; j++) {

				System.out.print(arr[i][j] + "\t");
			}

			System.out.println();
		}

	}	

}
