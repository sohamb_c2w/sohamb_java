class Remark{
	public static void main(String[] args){
		
		char remark = 'i';

		if(remark == 'O'){
			System.out.println("Outstanding");
		}else if(remark == 'A'){
			System.out.println("Excellent");
		}else if(remark == 'B'){
			System.out.println("Good");
		}else if(remark == 'C'){
			System.out.println("Average");
		}else if(remark == 'P'){
			System.out.println("Pass");
		}else if(remark == 'F'){
			System.out.println("Fail");
		}else{
			System.out.println("Invalid Remark");
		}
	}
}

