class TwoNumber{
        public static void main(String[] args){

 		int num1 = 45;
  		int num2 = 21;
				
 		if(num1 >= 0 &&  num2 >= 0){
			int product = num1 * num2;
			if(product % 2 == 0){
				System.out.println("Even");
			}else{
				System.out.println("Odd");
			}
		}else{
			System.out.println("Negative number not allowed");
		}

        }
}
