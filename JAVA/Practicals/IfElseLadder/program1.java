class OddEven{
	public static void main(String[] args){
		
		int number = 87;

		if(number % 2 == 0){
			System.out.println("Number is Even");
		}else if(number % 2 != 0){
			System.out.println("Number is Odd");
		}else{
			System.out.println("Invalid number");
		}
	}
}
