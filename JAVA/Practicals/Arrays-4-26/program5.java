import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter number of rows: ");
                int size = sc.nextInt();

                int arr[] = new int[size];
                

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();
                        
                }

                System.out.println("Array elements are: ");

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }

                System.out.println();
		
		int left = 0;
		int right = arr.length-1;
		int temp = 0;

		while(left < right){
			
			temp = arr[left];
			arr[left]=arr[right];
			arr[right]=temp;

			left++;
			right--;
		}

                System.out.println("Reversed array is: ");
		
		for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");

		}
		
		System.out.println();

        }

}
