import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter number of rows: ");
                int size = sc.nextInt();

                int arr[] = new int[size];
                
                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();
                
                }

                System.out.println("Array elements are: ");

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }
			
		System.out.println();
                System.out.println();

		System.out.print("Enter a number to check: ");
		int num = sc.nextInt();
		
		int count = 0;

		for(int i = 0 ; i< arr.length ; i++){
			
			if(arr[i] == num)
				count++;

		}


		if(count > 2)
                	System.out.println(num + " occurs more than 2 times in the array");
		else if(count == 2)
			System.out.println(num + " occurs 2 times");
		else
			System.out.println(num + " doesn't occurs more than 2 times");
        }

}
