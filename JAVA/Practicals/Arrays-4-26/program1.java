import java.util.*;
class Arrays{
	
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter number of rows: ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		int sum = 0;

		for(int i = 0 ; i < arr.length ; i++){
			
			System.out.print("Enter element ["+i+"]: ");
			arr[i] = sc.nextInt();
			sum += arr[i];
		}

		System.out.println("Array elements are: ");

		for(int i = 0 ; i < arr.length ; i++){
			
			System.out.print(arr[i] + " | ");
		}

		System.out.println();
		System.out.println("Average of Array elements is: "+(sum/arr.length));


	}

}
