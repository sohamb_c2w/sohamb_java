import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter number of rows: ");
                int size = sc.nextInt();

                int arr[] = new int[size];
                int max = 0;
		int secMax = 0;
		int temp = 0;

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();
                        
			if(arr[i] >= max){
				
				max = arr[i];

			}

			if(arr[i] <= max){
			
				temp = arr[i];
			}

                }

                System.out.println("Array elements are: ");
				

                for(int i = 0 ; i < arr.length ; i++){

			if(arr[i] > temp && arr[i] < max){
				secMax = arr[i];
			}
                
			System.out.print(arr[i] + " | ");
                }

                System.out.println();
                System.out.println("The second largest element from array is: "+secMax);


        }

}
