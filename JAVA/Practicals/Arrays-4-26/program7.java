import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter number of rows: ");
                int size = sc.nextInt();

                char arr[] = new char[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.next().charAt(0);
                        
                }

                System.out.println("Array elements are: ");

                for(int i = 0 ; i < arr.length ; i++){

                       System.out.print(arr[i] + " | ");
	
			if(arr[i] <= 122 && arr[i] >= 97)
				arr[i] -= 32;
                }

                System.out.println();
		
		System.out.println("New array: ");

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }
                System.out.println();
		

        }

}
