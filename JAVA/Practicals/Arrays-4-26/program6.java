import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter number of rows: ");
                int size = sc.nextInt();

                char arr[] = new char[size];
        	
		int vowels = 0;
		int consonants = 0;	
	
                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.next().charAt(0);

			if(arr[i] == 'a' || arr[i] == 'A' || arr[i] == 'e' || arr[i] == 'E' || arr[i] == 'i' || arr[i] == 'I' || arr[i] == 'o' || arr[i] == 'O' || arr[i] == 'u' || arr[i] == 'U')
				vowels++;
			else
				consonants++;

                }

                System.out.println("Array elements are: ");

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");

                }

		System.out.println();
		System.out.println("Count of vowels is: "+vowels);
		System.out.println("Count of consonants is: "+consonants);

                

        }

}
