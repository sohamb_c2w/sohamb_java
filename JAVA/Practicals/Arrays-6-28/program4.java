import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array: ");
                int size = sc.nextInt();

                int arr1[] = new int[size];
		
		System.out.println("***Array1***");
                System.out.println();
                for(int i = 0 ; i < arr1.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr1[i] = sc.nextInt();

                }

                System.out.println();

                for(int i = 0 ; i < arr1.length ; i++){

                        System.out.print(arr1[i] + " | ");
                }

                System.out.println();

                int arr2[] = new int[size];

		System.out.println("***Array2***");

                System.out.println();
                for(int i = 0 ; i < arr2.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr2[i] = sc.nextInt();

                }

                System.out.println();

                for(int i = 0 ; i < arr2.length ; i++){

                        System.out.print(arr2[i] + " | ");
                }

                System.out.println();

		System.out.println("Common elements in both arrays are: ");

		for(int i = 0 ; i < arr1.length ; i++){
		
			for(int j = 0 ; j < arr2.length ; j++){
				
				if(arr1[i]==arr2[j])
					System.out.print(arr1[i]+" , ");
			}
		
		}

		System.out.println();

                
  	}
}
