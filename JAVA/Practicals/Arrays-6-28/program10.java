import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array: ");
                int size = sc.nextInt();

                int arr[] = new int[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();

                }

                System.out.println();

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }

                System.out.println();

		int max = arr[0] ; 
		int secMax = arr[0];
		int thirdMax = arr[0];


                for(int i = 0 ; i < arr.length ; i++){
			
			if(arr[i] > max){
				
				thirdMax = secMax;
				secMax = max;
				max = arr[i];

			}else if(arr[i] < max && arr[i] > secMax){
				
				thirdMax = secMax;
				secMax = arr[i];

			}else if(arr[i]	< secMax && arr[i] > thirdMax){
				
				thirdMax = arr[i];
			}

		}
		System.out.println("third largest element is : "+thirdMax);
        }
}                    
