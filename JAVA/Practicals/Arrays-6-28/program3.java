import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array: ");
                int size = sc.nextInt();

                int arr[] = new int[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();

                }

                System.out.println();

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }

                System.out.println();

		System.out.print("Enter the key: ");
		int key = sc.nextInt();
		
		int count = 0;

                for(int i = 0 ; i < arr.length ; i++){
			
			if(arr[i] == key)
				count++;    
	
		}       

		if(count > 2){
		
			for(int i = 0 ; i < arr.length ; i++){
				
				if(arr[i]==key){
					
					System.out.print(arr[i]*arr[i]*arr[i]+" | ");
				}else{
					System.out.print(arr[i] + " | ");
				}
			}

		
		}else{
			
			System.out.println("key doesn't appear more than two times");
		}


	System.out.println();	

        }
}
