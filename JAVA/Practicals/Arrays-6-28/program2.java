import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array: ");
                int size = sc.nextInt();

                int arr[] = new int[size];
	
		int sum = 0 ;
		int count = 0 ;

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();

			int temp = 1;
			int cnt = 0;

			while(temp <= arr[i]/2){
				
				if(arr[i] % temp == 0 ){
					
					cnt++;
		
				}
				temp++;
			}

			if(cnt == 1){
				
				sum += arr[i];
				count++;
			}

                }

                System.out.println();

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }

                System.out.println();

                System.out.println("Sum of all prime number is "+sum+" and count of prime number is "+count);

        }
}
