class grades{
	public static void main(String[] args){
		
		char Remark = 'P';

		switch(Remark){
			
			case 'O':
				System.out.println("Outstanding");
				break;


			case 'A':
				System.out.println("Excellent");
				break;

			case 'B':
				System.out.println("Good");
				break;

			case 'C':
				System.out.println("Distinction");
				break;

			case 'D':
				System.out.println("Average");
				break;

			case 'P':
				System.out.println("Pass");
				break;

			case 'F':
				System.out.println("Fail");
				break;

			default:
				System.out.println("Enter valid Remark");
			
		}
	}
}
