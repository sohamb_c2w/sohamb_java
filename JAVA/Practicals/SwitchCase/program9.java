class Marks{
	public static void main(String[] args){
		
		int Sub1Marks = 54;
		int Sub2Marks = 87;
		int Sub3Marks = 41;
		int Sub4Marks = 76;
		int Sub5Marks = 16;

		if( Sub1Marks >= 35 && Sub2Marks >= 35 && Sub3Marks >= 35 && Sub4Marks >= 35 && Sub5Marks >= 35 ){
			
			int total = Sub1Marks + Sub2Marks + Sub3Marks + Sub4Marks + Sub5Marks ;
			int Avg = total/5;
			char Grade = '#';
			
			if(Avg >= 75){
				Grade = 'D';
			}else if(Avg >= 50 && Avg < 75){
				Grade = 'F';
			}else{
				Grade = 'S';
			}
			
			switch(Grade){
				
				case 'D':
					System.out.println("First class with Distinction");
					break;

				case 'F':
					System.out.println("Second class");
					break;
				
				case 'S':
					System.out.println("First class");
					break;		


			}
		}else{
			if(Sub1Marks < 35){
				System.out.println("You're Failed in Subject 1");
			}

			else if(Sub2Marks < 35){
				System.out.println("You're Failed in Subject 2");
			}
			else if(Sub3Marks < 35){
				System.out.println("You're Failed in Subject 3");
			}
			else if(Sub4Marks < 35){
				System.out.println("You're Failed in Subject 4");
			}
			else if(Sub5Marks < 35){
				System.out.println("You're Failed in Subject 5");
			}
		}
	}
}
