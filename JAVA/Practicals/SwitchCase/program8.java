class TwoNum{
	public static void main(String[] args){
		
		int num1 = -45;
		int num2 = 3;

		if( num1 >= 0 && num2 >= 0){
			int product = num1*num2;
			char type = '#';

			if( product % 2 == 0){
				type = 'E';
			}else{
				type = 'O';
			}

			switch(type){
				
				case 'O':
					System.out.println("ODD");
					break;

				case 'E':
					System.out.println("EVEN");
					break;
			}
		}else{
			System.out.println("Sorry negative numbers not allowed");
		}
	}
}
