class RealTimeEx{
	public static void main(String[] args){
		
		int BatsmanScore = 300;

		switch(BatsmanScore){
			
			case 0:
				System.out.println("Duck");
				break;

			case 50:
				System.out.println("Half Century");
				break;

			case 100:
				System.out.println("Century");
				break;

			case 200:
				System.out.println("Double century");
				break;

			case 300:
				System.out.println("Triple century");
				break;

			default:
				System.out.println("Batsman Score is " + BatsmanScore);
				break;
		}
	}
}
