import java.util.*;
class MixedPattern{
	public static void main(String[] args){
	
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the number of rows : ");
		int rows = sc.nextInt();
		int num =1;
		for(int i=1;i<=rows;i++){
			num = i;
			for(int j=1;j<=rows-(rows-i+1);j++){
					System.out.print("  ");
			}
			for(int j=1;j<=rows-(i)+1;j++){

					if(rows%2==1)
						System.out.print((char)(num++ +64)+" ");
					else
						System.out.print((char)(num++ +96)+" ");
					
			}
		//	num = i;
							
			System.out.println();
		}
	}
}
