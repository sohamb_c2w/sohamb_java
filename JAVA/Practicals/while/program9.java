class EvenOdd{
        public static void main(String[] args){

                int rem = 0;
                int num = 214367689;
		int odd = 0;
		int even = 0;

                while(num>0){

                        rem = num % 10 ;
                        num = num / 10;
			
			if(rem % 2 == 0)
                        	even++;
			else
				odd++;
                }

		System.out.println("even digits are "+even);
		System.out.println("odd digits are "+odd);
        }
}
