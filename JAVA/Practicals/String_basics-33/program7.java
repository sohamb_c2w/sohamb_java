import java.util.*;

class BasicString {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter string: ");
                String str1 = sc.next();
                System.out.println(str1);

                System.out.println("Uppercased string is: " + str1.toUpperCase());
        }
}
