import java.util.*;

class BasicString {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter string1: ");
                String str1 = sc.next();
                System.out.println(str1);

                System.out.print("Enter string2: ");
                String str2 = sc.next();
		System.out.println(str2);

		String str3 = str1.concat(str2);

		System.out.println("concated string is: " + str3);
		System.out.println("Length of string is: " + str3.length());
        }
}
