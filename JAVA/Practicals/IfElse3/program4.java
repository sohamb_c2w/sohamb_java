class IfElseDemo{
        public static void main(String[] args){

		char ch = 97;			//a
		System.out.println(ch);		//a
		if(ch == 'a'){
			ch+=3;			//(100)
			System.out.println(ch--);//d(100)
		}else{
			System.out.println(ch++);
		}

		System.out.println(ch+=5);	//h(104)
        }
}
