class IfElseDemo{
        public static void main(String[] args){

                int age = -9;
		if(age>0){
			if(age >= 18){
				System.out.println("Eligible to vote");
			}else{
				System.out.println("Not eligible to vote");
			}
		}else{
			System.out.println(age + " is not valid age. Enter age again");
		}
        }
}
