class IfElseDemo{
        public static void main(String[] args){

                float percentage = 75.25f;
                if(percentage >= 85.00){
                        System.out.println("You can go for MEDICAL");
                }else if(percentage < 85.00 && percentage >= 65.00){
                        System.out.println("You can go for ENGINEERING");
                }else if(percentage > 65.00 && percentage <= 35.00){
                        System.out.println("You can go for PHARMACY or BACHELOR IN SCIENCE");
                }
        }
}
