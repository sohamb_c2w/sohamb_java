class IfElseDemo{
        public static void main(String[] args){

                char alpha = 'a';

                if(alpha == 'a' || alpha == 'e' || alpha == 'i' || alpha == 'o' || alpha == 'u' || alpha == 'A' || alpha == 'E' || alpha == 'I' || alpha == 'O' || alpha == 'U'){
                        System.out.println(alpha + " is vowel");
                }else{
                        System.out.println(alpha + " is consonant");
             	}
        }
}
