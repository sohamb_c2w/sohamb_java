class IfElseDemo{
        public static void main(String[] args){

                int num = 26;

                if(num % 13 == 0){
                        System.out.println(num + " is in the table of 13");
                }else if(num % 13 != 0){
                        System.out.println(num + " is not in table of 13");
                }else{
                        System.out.println("invalid number");
                }
        }
}
