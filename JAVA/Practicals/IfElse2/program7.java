class IfElseDemo{
        public static void main(String[] args){

                float costPrice = 1000.5f;
		float sellingPrice = 1200.58f;

                if(costPrice < sellingPrice){
                        System.out.println("Profit of "+(sellingPrice-costPrice));
                }else if(costPrice > sellingPrice){
                        System.out.println("Loss of "+(costPrice-sellingPrice));
                }else{
                        System.out.println("No Profit,No Loss");
                }
        }
}
