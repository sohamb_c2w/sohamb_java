class IfElseDemo{
        public static void main(String[] args){

                float percentage = 65.36f;;

                if(percentage >= 75){
                        System.out.println("First class with distinction");
                }else if(percentage < 75 && percentage >= 60){
                        System.out.println("First class");
                }else if(percentage < 60 && percentage >= 50){
                        System.out.println("Second class");
                }else if(percentage < 50 && percentage >= 35){
			System.out.println("Pass");
		}else{
			System.out.println("Fail");
		}
        }
}
