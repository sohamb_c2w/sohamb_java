class IfElseDemo{
	public static void main(String[] args){
		
		int num = 29;
		
		if(num >= 1 && num <= 1000){
			System.out.println(num+" is in the range 1 to 1000");
		}else if(num <= 1 && num >= 1000){
			System.out.println(num + " is not in range 1 to 1000");
		}else{
			System.out.println("invalid number");
		}
	}
}
