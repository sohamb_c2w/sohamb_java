import java.io.*;

class Pattern{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter number of rows: ");
                int rows = Integer.parseInt(br.readLine());

                int ch = rows;

                for(int i = 1 ; i<= rows ; i++){

                        for(int j = 1 ; j<= rows ; j++){

                                if(j>(rows-i))
                                        System.out.print((char)(64+ch)+" ");
                                else
                                        System.out.print((char)(96+ch)+" ");

                                ch++;
                        }
                        System.out.println();
                }



        }
}
