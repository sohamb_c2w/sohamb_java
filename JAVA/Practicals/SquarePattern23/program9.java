import java.io.*;

class Pattern{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter number of rows: ");
                int rows = Integer.parseInt(br.readLine());

		int num = rows*rows;
                

                for(int i = 1 ; i<= rows ; i++){

                        for(int j = 1 ; j<= rows ; j++){

                                if(j % 2 == 0)
                                        System.out.print("@\t");                                else
                                        System.out.print( num*i +"\t");
                               
				num--;
                        }
                        System.out.println();
                }



        }
}
