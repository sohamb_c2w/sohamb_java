import java.io.*;

class Pattern{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                                System.out.print("enter number of rows : ");
                                int rows = Integer.parseInt(br.readLine());

                                for(int i = 1 ; i<=rows ; i++){
					
					int ch = rows;

                                        for(int j = 1 ; j<=i ;j++){
						
						if(i%2==0){
                                        	        System.out.print((char)(ch+64) + " ");
							ch--;
						}else{
							System.out.print((char)(96+ch)+" ");
							ch--;
						}
                                        }
                                        System.out.println();
                                }
        }
}
