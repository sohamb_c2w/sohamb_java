import java.io.*;

class Pattern{

        public static void main(String[] args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter number of rows: ");
                int rows = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= rows ; i++){

                        for(int j = 1 ; j < i ; j++){

                                System.out.print("  ");
                        }

			int num = 64+i;

                        for(int k = 1 ; k <= rows-i+1 ; k++ ){

				if((k%2==0 && i%2==1) || (k%2==1 && i%2==0)){
					
					System.out.print((char)num++ + " ");
				}else{
					
					System.out.print(num++ + " ");
				}
			
				
                                
                        }

                        System.out.println();
                }
        }
}
