class NestedFor{
        public static void main(String[] args){

                int rows = 3;
		int num = 1;
                for( int i = 1 ; i <= rows ; i++ ){
                        for ( int j = 1 ; j <= rows ; j++){

                                System.out.print((char)(64+num) + " ");
				num += 2;
                        }
                        System.out.println();
                }

        }
}
