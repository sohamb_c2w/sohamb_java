import java.util.*;

class Daimond {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter number of rows: ");
                int rows = sc.nextInt();

                int col = 0;
                int val = 0;
               	char ch = 'A';

                for(int i = 1 ; i < 2*rows ; i++) {


                        if(i <= rows){

                                col = rows-i;
                                val = 2*i-1;
                             	ch = 'A';

                        }else{

                                col = i-rows;
                                val = val-2;
                               	ch = 'A';

                        }

                        for(int j = 1 ; j <= col ; j++) {

                                System.out.print("\t");
                        }

                        for(int j = 1 ; j <= val ; j++) {

                                if(j <= val/2){
                                        System.out.print(ch++ + "\t");

                                }else{
                                        System.out.print(ch-- + "\t");


                                }
                        }

                        System.out.println();

                }
        }
}
