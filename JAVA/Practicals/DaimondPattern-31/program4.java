import java.util.*;

class Daimond {

        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter number of rows: ");
                int rows = sc.nextInt();

                int col = 0;
                int val = 0;
                int num = 0;

                for(int i = 1 ; i < 2*rows ; i++) {


                        if(i <= rows){

                                col = rows-i;
                                val = 2*i-1;
                                num = 1;

                        }else{

                                col = i-rows;
                                val = val-2;
                                num = 1;

                        }

                        for(int j = 1 ; j <= col ; j++) {

                                System.out.print("\t");
                        }

                        for(int j = 1 ; j <= val ; j++) {

                                if(j <= val/2){
                                        System.out.print(num++ + "\t");

                                }else{
                                        System.out.print(num-- + "\t");


                                }
                        }

                        System.out.println();

                }
        }
}
