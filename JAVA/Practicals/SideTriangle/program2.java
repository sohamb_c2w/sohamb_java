import java.util.*;
class SideTri{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter number of rows: ");
                int rows = sc.nextInt();

                int col = 0;

                for(int i = 1 ; i < 2*rows ; i++){

                        if(i <= rows){

                                col = i;

                        }else{

                                col--;
                        }

                        for(int j = 1 ; j <= col ; j++){

                                System.out.print( j + "  ");
                        }

                        System.out.println();
                }
        }
}
