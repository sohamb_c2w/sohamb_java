import java.util.*;
class SideTri{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter number of rows: ");
                int rows = sc.nextInt();

                int col = 0;
		int val = rows+1;

                for(int i = 1 ; i < 2*rows ; i++){

                        if(i <= rows){

                                col = i;
				val--;

                        }else{

                                col--;
				val = 2*rows-i;
                        }

                        for(int j = 1 ; j <= col ; j++){

                                System.out.print(val + "  ");
                        }

                        System.out.println();
                }
        }
}
