import java.util.*;
class SideTri{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter number of rows: ");
                int rows = sc.nextInt();

                int col = 0;
                int val = 0;

                for(int i = 1 ; i < 2*rows ; i++){

                        if(i <= rows){

                                col = rows - i ;
                        }else{

                                col = i - rows;
                        }

                        for(int j = 1 ; j <= col ; j++){

                                System.out.print("   ");
                        }

                        if(i <= rows){

                                col = i;
                                val = rows - col;

                        }else{

                                col = rows*2-i;
                                val = rows - col;
                        }

                        for(int j = 1 ; j <= col ; j++){

                                System.out.print((val++) + "  ");
                        }

                        System.out.println();
                }
        }
}
