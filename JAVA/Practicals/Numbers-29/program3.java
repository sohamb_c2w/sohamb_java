import java.util.*;
class Deficient_number{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter the number: ");
                int num = sc.nextInt();

                int temp = 1;
                int sum = 0;

                while( num/2 >= temp){

                        if(num % temp == 0){

                                sum += temp;
                        }

                        temp++;
                }

                if( sum < num)
                        System.out.println(num + " is a Deficient number");
                else
                        System.out.println(num + " is not a Deficient number");

        }

}
