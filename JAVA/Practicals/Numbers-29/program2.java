import java.util.*;
class Strong_number{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter the number: ");
                int num = sc.nextInt();
		
		int var = num ;
                int sum = 0;
		int rem = 0;

                
		while( num > 0){

                	
			int fact = 1 ;
                	int temp = 1;

			rem = num % 10;
			
			while(rem >= temp){
					
				fact *= temp;
				temp++;
			}

			sum += fact;

			num /= 10;	
                }

                if( sum == var)
                        System.out.println(var + " is a Strong number");
                else
                        System.out.println(var + " is not a Strong number");

        }

}
