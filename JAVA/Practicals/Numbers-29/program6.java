import java.util.*;
class Fibonacci_series{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter the number: ");
                int num = sc.nextInt();

               	int arr[] = new int[num];

		arr[0] = 0;
		arr[1] = 1;

		for(int i = 2 ; i < arr.length ; i++){
		
			arr[i] = arr[i-1] + arr[i-2];
		}

		System.out.println("Fibonacci series : ");

		for(int i = 0 ; i < arr.length ; i++){
			
			System.out.print(arr[i] + " , ");
		}
		
		System.out.println();


        }

}
