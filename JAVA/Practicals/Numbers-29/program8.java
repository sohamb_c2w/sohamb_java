import java.util.*;
class Duck_number{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter the number: ");
                int num = sc.nextInt();

                int var = num;

		boolean flag = false;
		boolean start = false;
                int rem = 0;

                while( num > 0){

                        rem = num % 10;
			
			if(rem == 0){
				
				flag = true;
			}else if(rem != 0 && flag ){
				
				start = true;
			}


			num /= 10;
			
                }

                if(flag && start)
                        System.out.println(var + " is a Duck number");
                else
                        System.out.println(var + " is not a Duck number");

        }

}
