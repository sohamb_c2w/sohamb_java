import java.util.*;
class Automorphic_number{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.print("Enter the number: ");
                int num = sc.nextInt();

		int var = num;

		int square = num*num;

                int temp = 1;
              	int remSq = 0;
		int rem = 0;
		boolean check = true;

                while( num > 0){
			
			rem = num % 10;
			remSq = square % 10;

			if(rem == remSq)
				check = true;
			else
				check = false;

			num /= 10;
			square /= 10;
                        
 		}

                if(check)
                        System.out.println(var + " is a Automorphic number");
                else
                        System.out.println(var + " is not a Automorphic number");

        }

}
