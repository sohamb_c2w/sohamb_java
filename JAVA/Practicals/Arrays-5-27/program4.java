import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array: ");
                int size = sc.nextInt();

                int arr[] = new int[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();

                }

                System.out.println();

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }

                System.out.println();

                int count = 0;
		boolean flag = false;
                for(int i = 0 ; i < arr.length ; i++){

                        for(int j = i+1 ; j < arr.length ; j++){
				
				if(arr[i] == arr[j]){
					System.out.println("First duplicate element found at index: "+i);
					flag = true;
					break;
				}

			
			}
				if(flag){
					
					break;
				}
                }

                
	}
}
