import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);


        	System.out.print("Enter the number: ");
		int num = sc.nextInt();
	       
		int rem = 0;
		int count = 0;
		
		int value = num;
		while(num > 0){
		
			num = num / 10;
			count++;
		}
		
		int arr[] = new int[count];
		
		int index = arr.length-1;
		
		while(value > 0){
		
			rem = value % 10;
			arr[index] = rem + 1;
			value = value / 10;
			index--;
		}

                System.out.println();

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }

                System.out.println();

                
        }
}
