import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array: ");
                int size = sc.nextInt();

                int arr[] = new int[size];

		int oddSum = 0;
		int evenSum = 0;
                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();

			if(arr[i] % 2 == 1 )
				oddSum += arr[i];
			else
				evenSum += arr[i];

                }

                System.out.println();

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }

                System.out.println();

                System.out.println("Odd Sum = " + oddSum);
                System.out.println("Even Sum = " + evenSum);

        }
}
