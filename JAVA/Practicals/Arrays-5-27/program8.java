import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array: ");
                int size = sc.nextInt();

                int arr[] = new int[size];
		
                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();

                }

                System.out.println();

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }

                System.out.println();

                int min = arr[0];
		int secMin = 0;
		int temp = 0;

                for(int i = 0 ; i < arr.length ; i++){

                        if(arr[i] <= min)
				min = arr[i];

			if(min <= arr[i])
				temp = arr[i];

   
                }

		for(int i = 0 ; i < arr.length ; i++){
			
			if(arr[i] <= temp && arr[i] >= min)
				temp = arr[i];
			secMin = temp;
		}

                System.out.println("The second minimum element in the array is: "+secMin);
                
        }
}
