import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array: ");
                int size = sc.nextInt();

                int arr[] = new int[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();

                }

                System.out.println();

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }

                System.out.println();


                for(int i = 0 ; i < arr.length ; i++){

                        int temp = 1;
                	int count = 0;
		
			while(temp <= arr[i]/2){
			
				if(arr[i] % temp == 0){
					
					count++;
				}

				temp++;
				
			}

			if(count == 1){
				
				System.out.println("First prime number found at index: " + i);
				break;
			}


                }

                
        }
}
