import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter size of array: ");
                int size = sc.nextInt();

                int arr[] = new int[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"]: ");
                        arr[i] = sc.nextInt();

                }

                System.out.println();

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }

                System.out.println();

                int left = 0;
		int right = arr.length-1;
		int count = 0;

                while(left < right){
			
			if(arr[left] == arr[right])
				count++;

			left++;
			right--;

		}


                if(count == arr.length/2)
                        System.out.println("The given array is a palindrome");
                else
                        System.out.println("The given array is not a palindrome");
        }
}
