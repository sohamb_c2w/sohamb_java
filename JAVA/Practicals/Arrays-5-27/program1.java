import java.util.*;
class Arrays{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for(int i = 0 ; i < arr.length ; i++){
			
			System.out.print("Enter element ["+i+"]: ");
			arr[i] = sc.nextInt();

		}

		System.out.println();

		for(int i = 0 ; i < arr.length ; i++){
			
			System.out.print(arr[i] + " | ");
		}

		System.out.println();

		int count = 0;

		for(int i = 0 ; i < arr.length-1 ; i++){
			
			if(arr[i] <= arr[i+1])
				count++;

		}

		if(count == arr.length-1)
			System.out.println("The given array is in ascending order");
		else
			System.out.println("The given array is not in ascending order");
	}
}
