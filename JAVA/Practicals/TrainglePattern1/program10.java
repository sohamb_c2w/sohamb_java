import java.io.*;

class pattern{
        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter number of Rows : ");

                int rows = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= rows ; i++){
			int num = 64+i;
                        for(int j = rows ; j >= i ; j--){
				
				if( i % 2 == 0){
					if(j % 2 != 0){	
                                		System.out.print(num + " ");
						num++;
					}else{
                                		System.out.print((char)(num) + " ");
						num++;
					}

				}else{
					 if(j % 2 != 0){
                                                System.out.print((char)(num) + " ");
						num++;
                                        }else{
                                                System.out.print(num + " ");
						num++;
                                        }
				}
                        }

                        System.out.println();

                }
        }
}
