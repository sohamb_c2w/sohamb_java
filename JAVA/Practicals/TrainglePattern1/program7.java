import java.io.*;

class pattern{
        public static void main(String[] args) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter number of Rows : ");

                int rows = Integer.parseInt(br.readLine());

                for(int i = 1 ; i <= rows ; i++){
			
			int num = 1;

                        for(int j = rows ; j >= i ; j--){

                                System.out.print(num++ + " ");
                        }

                        System.out.println();

                }
        }
}
