import java.util.*;
class Pattern{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.print("Enter number of rows: ");
                int rows = sc.nextInt();


                for(int i = 1 ; i <= rows ; i++){


                        for(int j = 1 ; j < i ; j++){

                                System.out.print("   ");
                        }

                	int num = 1;

                        for(int j = 1 ; j <= 2*rows-2*i+1 ; j++){
				
				if(j < rows-i+1){
                                        System.out.print((char)(num +64) + "  ");
					num++;
				}else{
					System.out.print((char)(num +64) +"  ");
					num--;
				}
                        }

                        System.out.println();
                }
        }
}
