import java.util.*;
class Pattern{

        public static void main(String[] args){

                Scanner sc = new Scanner (System.in);
                System.out.print("Enter number of rows: ");
                int rows = sc.nextInt();

                for(int i = 1 ; i <= rows ; i++){

                        for(int j = 1 ; j < i ; j++){

                                System.out.print("   ");
                        }

                                int num = i;

                        for(int j = 1 ; j <= 2*rows-2*i+1 ; j++){

                                if(j % 2 == 0)
                                        System.out.print("0  ");
                                else
                                        System.out.print("1  ");
                        }

                        System.out.println();
                }

        }

}
