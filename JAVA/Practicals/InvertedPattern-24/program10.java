import java.util.*;
class Pattern{

        public static void main(String[] args){

                Scanner sc = new Scanner (System.in);
                System.out.print("Enter number of rows: ");
                int rows = sc.nextInt();

		int num = rows;
                for(int i = 1 ; i <= rows ; i++){

			int ro = num;
                        
			for(int j = 1 ; j < i ; j++){

                                System.out.print("  ");
                        }

                        for(int j = 1 ; j <= 2*rows-2*i+1 ; j++){

                                if(j < rows-i+1)
                                        System.out.print(ro-- + " ");
                                else
                                        System.out.print(ro++ + " ");
                        }
			num--;
                        System.out.println();
                }

        }

}
