import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner (System.in);

                System.out.print("Enter array size: ");
                int size = sc.nextInt();

                int arr[] = new int[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"] :");
                        arr[i] = sc.nextInt() ;

                }

                System.out.println("Array elements are: ");
                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }
                System.out.println();
               
 		int product = 1;
 
		for(int i = 0 ; i < arr.length ; i++){

			int temp = 1;
			int count = 0;

			while(arr[i]/2 >= temp){
				
				if(arr[i] % temp == 0 ){
					
					count++;	
				}

				temp++;

			}

			if(count == 1 ){
				
				product *= arr[i];
			}
 						
 		}


		System.out.println("Product of Prime numbers from arrays is: "+product);
                System.out.println();

        }
}
