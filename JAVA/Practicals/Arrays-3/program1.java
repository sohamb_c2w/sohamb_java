import java.util.*;
class Arrays{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner (System.in);

		System.out.print("Enter array size: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		for(int i = 0 ; i < arr.length ; i++){
			
			System.out.print("Enter element ["+i+"] :");
			arr[i] = sc.nextInt() + 15;
			
		}

		System.out.println("Array elements are: ");
		for(int i = 0 ; i < arr.length ; i++){
			
			System.out.print(arr[i] + " | ");
		}
		
		System.out.println();

	}
}
