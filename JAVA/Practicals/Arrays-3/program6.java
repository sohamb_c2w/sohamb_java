import java.util.*;
class Arrays{

        public static void main(String[] args){

                Scanner sc = new Scanner (System.in);

                System.out.print("Enter array size: ");
                int size = sc.nextInt();

                char arr[] = new char[size];

                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print("Enter element ["+i+"] :");
                        arr[i] = sc.next().charAt(0) ;

                }

                System.out.println("Array elements are: ");
                for(int i = 0 ; i < arr.length ; i++){

                        System.out.print(arr[i] + " | ");
                }
                System.out.println();

                for(int i = 0 ; i < arr.length ; i++){

                        if(arr[i]!='a' && arr[i]!='e' && arr[i]!='i' && arr[i]!='o' && arr[i]!='u'){
                                System.out.print(arr[i] + " ");
             
                        }
                }


                System.out.println();

        }
}
